package com.wiprodigital.accountopening;

import java.util.ArrayList;
import java.util.List;

import org.mule.module.apikit.exception.MuleRestException;
import org.mule.module.apikit.exception.NotFoundException;

import com.wiprodigital.accountopening.model.Customer;
import com.wiprodigital.accountopening.model.CustomerDB;

public class CustomerAPI {
    private CustomerDB customerDB;

    public void initialize() {
        customerDB = new CustomerDB();
        customerDB.initialize();
    }

    public Customer getCustomer(String customerId) throws MuleRestException {
    	if (!customerDB.hasCustomer(customerId)) {
    		throw new NotFoundException("Customer " + customerId + " does not exist");
    	}
    	return customerDB.getCustomer(customerId);
    }
    
    public List<Customer> getCustomer(String firstName, String lastName) {
    	ArrayList<Customer> customers = new ArrayList<Customer>();
    	customers.add(customerDB.getCustomer(firstName, lastName));
    	return customers;
    	//return customers.toArray(new Customer[customers.size()]);
    	//return new Customer[0];
    	//return new ArrayList<Customer>();
    }
}
