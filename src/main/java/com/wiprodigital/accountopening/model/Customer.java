package com.wiprodigital.accountopening.model;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Customer {    	
    private String cin;
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private String address;
    private String postCode;
    private String city;

    @JsonProperty
    public String getCin() {
		return cin;
	}

	public Customer setCin(String cin) {
		this.cin = cin;
		return this;
	}

	@JsonProperty
	public String getFirstName() {
		return firstName;
	}

	public Customer setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	@JsonProperty
	public String getLastName() {
		return lastName;
	}

	public Customer setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}
	
	@JsonProperty
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public Customer setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
		return this;
	}

	@JsonProperty
	public String getAddress() {
		return address;
	}

	public Customer setAddress(String address) {
		this.address = address;
		return this;
	}

	@JsonProperty
	public String getPostCode() {
		return postCode;
	}

	public Customer setPostCode(String postCode) {
		this.postCode = postCode;
		return this;
	}

	@JsonProperty
	public String getCity() {
		return city;
	}

	public Customer setCity(String city) {
		this.city = city;
		return this;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;
        if (cin != null ? !cin.equals(customer.cin) : customer.cin != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return cin != null ? cin.hashCode() : 0;
    }
}