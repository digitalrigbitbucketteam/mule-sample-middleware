package com.wiprodigital.accountopening.model;

import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class CustomerDB {
    private static final Logger LOGGER = Logger.getLogger(CustomerDB.class);

    private HashMap<String, Customer> customers = new HashMap<String, Customer>();

    @SuppressWarnings("unchecked")
    public void initialize() {
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject jsonObject = (JSONObject) jsonParser.parse(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("customers.json")));
            JSONArray customers = (JSONArray) jsonObject.get("customers");
            Iterator<JSONObject> iterator = customers.iterator();
            while(iterator.hasNext()) {
                addCustomer(iterator.next());
            }
        } catch (Exception e) {
            LOGGER.error("Error initializing customers. Cause: ", e);
        }
    }

    private void addCustomer(JSONObject jsonObject) {
        Customer customer = new Customer();
        customer.setCin((String) jsonObject.get("cin"));
        customer.setFirstName((String) jsonObject.get("firstname"));
        customer.setLastName((String) jsonObject.get("lastname"));
        customer.setDateOfBirth((String) jsonObject.get("dateofbirth"));
        customer.setAddress((String) jsonObject.get("address"));
        customer.setPostCode((String) jsonObject.get("postcode"));
        customer.setCity((String) jsonObject.get("city"));
        this.customers.put(customer.getCin(), customer);
    }

    public boolean hasCustomer(String cin) {
        return customers.containsKey(cin);
    }
    
    public Customer getCustomer(String cin) {
    	return customers.get(cin);
    }
    
    public Customer getCustomer(String firstName, String lastName) {
    	for (Customer customer : customers.values()) {
    		if (firstName.equals(customer.getFirstName()) && lastName.equals(customer.getLastName())) {
    			return customer;
    		}
    	}
    	return null;
    }
}
