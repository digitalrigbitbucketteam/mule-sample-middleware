package com.wiprodigital.accountopening;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mule.api.MuleMessage;
import org.mule.api.config.MuleProperties;
import org.mule.module.client.MuleClient;
import org.mule.tck.junit4.FunctionalTestCase;

import com.wiprodigital.accountopening.model.Customer;

public class ApiFunctionalTest extends FunctionalTestCase {
	private static final Logger log = LogManager.getLogger(ApiFunctionalTest.class);
	
	private static final String PATH_TO_TEST_PROPERTIES = "./src/test/resources/mule.test.properties";
	private static final String APP_HOME_DIRECTORY_PROPERTY = "./src/main/api/";
	private static final String APP_FILENAME_PROPERTY = "api.xml";

	private static String SERVER = "http://localhost";
	private static String PORT;

    private static String CUSTOMERS_URI = "/customers";

    @BeforeClass
	public static void init() {
    	final Properties props = new Properties();
    	try {
    		props.load(new FileInputStream(PATH_TO_TEST_PROPERTIES));
    	} catch (Exception e) {
    		log.error("Error occured while reading " + PATH_TO_TEST_PROPERTIES, e);
    	}
    	PORT = props.getProperty("http.port");
    	System.setProperty("http.port", props.getProperty("http.port"));
	}

    @Test
    public void retrieveCustomerFromFirstNameLastNameDOBAddress() throws Exception {
    	Customer existingCustomer = new Customer()
    			.setFirstName("Joe")
    			.setLastName("Bloggs")
    			.setDateOfBirth("13.01.1965")
    			.setAddress("1 George Street")
    			.setPostCode("W1 5TT")
    			.setCity("London");

        MuleClient client = new MuleClient(muleContext);

        MuleMessage result = client.send(SERVER + ":" + PORT + "/api" + CUSTOMERS_URI 
        		+ "?firstname=" + existingCustomer.getFirstName()
        		+ "&lastname=" + existingCustomer.getLastName()
        		+ "&dateofbirth=" + existingCustomer.getDateOfBirth()
        		+ "&address=" + existingCustomer.getAddress()
        		+ "&postcode=" + existingCustomer.getPostCode()
        		+ "&city=" + existingCustomer.getCity(),
        		"", method("GET"));

        assertEquals("200", result.getInboundProperty("http.status"));
        assertNotNull(result.getPayloadAsString());
    }
    
    @Override
    protected String getConfigResources() {
        return APP_FILENAME_PROPERTY;
    }
    
    @Override
	protected Properties getStartUpProperties() {
		Properties properties = new Properties(super.getStartUpProperties());
		properties.put(MuleProperties.APP_HOME_DIRECTORY_PROPERTY, APP_HOME_DIRECTORY_PROPERTY);
		return properties;
	}
    
    private Map<String, Object> method(String methodName) {
    	Map<String, Object> props = new HashMap<String, Object>();
        props.put("http.method", methodName);
        return props;
    }
}
