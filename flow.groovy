#!groovy

node {
    git url: 'git@bitbucket.org:digitalrigbitbucketteam/mule-sample-middleware.git', credentialsId: 'b60bdb34-d871-4665-8a3a-a1903097d222'

    mvn 'clean deploy -Dport=9992 -Dcloudhub.username=account-opening -Dcloudhub.password=zhye43HksUXrFPQV'

    step([$class: 'JUnitResultArchiver', testResults: 'target/surefire-reports/*.xml'])

    archive "target/*.zip"
}

def mvn(args) {
    sh "${tool 'M3'}/bin/mvn ${args}"
}
