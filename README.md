# Account Opening #

#### Local environment ####

- Download [Anypoint Studio](https://www.mulesoft.com/platform/studio)
- Download [Mule Enterprise Standalone 3.7.3](https://www.mulesoft.com/lp/dl/mule-esb-enterprise) and [populate](https://docs.mulesoft.com/mule-user-guide/v/3.7/maven-reference) your local M2 artifact repository to give Maven access to some necessary Mule Enterprise JARs
- Run `mvn eclipse:eclipse` to generate the files `.project` and `.classpath` for Anypoint Studio
- Run `mvn tests`

#### Staging environment ####

Any build that passes the tests is packaged and deployed on Nexus and CloudHub.

- [Staging API Console](http://account-opening-staging.cloudhub.io/api/console/)

#### Stable environment ####

This environment is updated manually from the CloudHub console when desired.
It will be fed by the artifact used in the Staging environment.

- [Stable API Console](http://account-opening.cloudhub.io/api/console/)

#### Local development/deployment environment ####

The project includes a Vagrant box configured with CentOS 7, Mule ESB and Mule MMC.

 * Create the Vagrant box, just run `cd vagrant && vagrant up`

 * After the creation of the Vagrant box, the Mule MMC is accessible on the following [URL](http://192.168.100.100:8585/mmc-3.7.0/).